<?php


namespace App\Models;


abstract class BaseObject
{
    /**
     * @return array
     */
    abstract function getFields(): array;

    /**
     * @return array
     */
    abstract function getJson(): string;

    /**
     * @param array $fields
     * @return mixed
     */
    abstract function setFields(array $fields);


}
