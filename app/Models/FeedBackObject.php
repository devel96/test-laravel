<?php


namespace App\Models;


use Illuminate\Support\Arr;

class FeedBackObject extends BaseObject
{
    private $fields = [
        'name' => '',
        'email' => '',
        'phone' => '',
        'message' => ''
    ];

    /**
     * FeedBackObject constructor.
     * @param array $fields
     */
    public function __construct(array $fields)
    {
        $this->setFields($fields);
    }

    /**
     * @param array $fields
     * @return $this|BaseObject
     */
    public function setFields(array $fields)
    {
        $this->fields = Arr::only($fields , ['name', 'email', 'phone', 'message']);

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getFields(): array
    {
        return $this->fields ;
    }

    /**
     * @return false|string
     */
    public function getJson(): string
    {
        return json_encode($this->fields);
    }
}
