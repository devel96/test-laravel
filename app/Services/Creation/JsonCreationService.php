<?php


namespace App\Services\Creation;


use Illuminate\Support\Facades\Storage;

class JsonCreationService extends BaseCreationService
{

    protected $path = 'feedbacks.json';

    public function save()
    {
        $json  = $this->data->getJson();

        if ( Storage::disk('public')->exists('feedbacks.json')) {
            $json =  $this->getmerged($json);
        }
        $data = Storage::disk('public')->put('feedbacks.json', $json);

        return $data;
    }

    /**
     * @param $json
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function getmerged($json)
    {
        $data =  Storage::disk('public')->get('feedbacks.json');
        $newdata[] =  json_decode($data, true);
        $newdata[] =  json_decode($json, true);

        $json = json_encode($newdata);
    }
}
