<?php


namespace App\Services\Creation\Feedback;


use App\Models\FeedBack;
use App\Services\Creation\DbCreationService;

class DbCreation extends DbCreationService
{
    protected $model = FeedBack::class;

}
