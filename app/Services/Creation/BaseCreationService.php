<?php


namespace App\Services\Creation;


use App\Models\BaseObject;

abstract class BaseCreationService
{

    protected BaseObject $data;

    public function __construct(BaseObject $data)
    {
        $this->data = $data;
    }

   abstract public function save();

}
