<?php


namespace App\Factories;

use App\Services\Creation\Feedback\JsonCreation;
 use App\Models\FeedBackObject;

 class FeedbackCreationFactory extends BaseFactory
{

     public function run($type = null)
     {
         $type = $type ?? $this->type;

         $class = "App\Services\Creation\Feedback\\".$type."Creation";

         try {
             $data = (new $class($this->data))->save();

         } catch (\Exception $exception) {
             abort(
               response()->json(['message' =>$exception->getMessage()], $exception->getCode() )
             );
         }
    }
}
