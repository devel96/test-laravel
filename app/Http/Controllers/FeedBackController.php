<?php

namespace App\Http\Controllers;

use App\Factories\FeedbackCreationFactory;
use App\Http\Requests\FeedbackCreationRequest;
use App\Models\FeedBackObject;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;

class FeedBackController extends Controller
{

    public function store(FeedbackCreationRequest $request)
    {
        $feedback = new FeedBackObject($request->all());
        $saveJson = Config::get('savedata.types.json');
        $saveDb = Config::get('savedata.types.database');

        (new FeedbackCreationFactory($feedback))->run($saveJson);
        (new FeedbackCreationFactory($feedback))->run($saveDb);

        return response()->json(['message' => 'Data saved in json file and in db'], Response::HTTP_CREATED);

    }
}
